# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from shutil import copyfile
import time
import os
import glob
import filecmp
from git import Repo
from goodtables import validate
import pandas as pd
from datetime import date

def createDriver():
    driver = webdriver.Chrome(executable_path=os.path.expanduser('~') +
                              r'\Downloads\chromedriver_win32\chromedriver.exe')
    return(driver)


def getFileName(driver):
    driver.get('chrome://downloads')
    return driver.execute_script("return document.querySelector('downloads-manager')"
                                 ".shadowRoot.querySelector('#downloadsList downloads-item')"
                                 ".shadowRoot.querySelector('div#content  #file-link').text")


def download(driver):
    folderPath = 'archive/'
    driver.get('https://covid.saude.gov.br/')
    button = driver.find_element(By.XPATH, "//ion-content//ion-button")
    try:
        updateTime_raw = driver.find_element(By.XPATH, "//ion-content/div[1]//*[contains(@class, 'lb-grey')]/span").text
    except:
        updateTime_raw = " arquivogeral"
    updateTime = updateTime_raw.split(' ')[0].replace("/","")
    print(updateTime)
    driver.implicitly_wait(10)
    ActionChains(driver).move_to_element(button).perform()
    driver.implicitly_wait(10)
    ActionChains(driver).move_to_element(button).click(button).perform()
    time.sleep(10)

    try:
        fileName = getFileName(driver)
        print(fileName)
#         copyfile(os.path.expanduser("~")+f'\\Downloads\\{fileName}', folderPath + fileName)
        copyfile(os.path.expanduser("~")+f'\\Downloads\\{fileName}', folderPath + f'HIST_PAINEL_COVIDBR_{updateTime}.xlsx')
        time.sleep(5)
        os.remove(os.path.expanduser("~")+f'\\Downloads\\{fileName}')
        print(f'Copied as HIST_PAINEL_COVIDBR_{updateTime}.xlsx')
    except:
        print('Nada feito.')
    driver.quit()

def extrairDiario(reportdate):
    file = f'archive\\HIST_PAINEL_COVIDBR_{reportdate}.xlsx'
    dfraw = pd.read_excel(file, parse_dates = ['data'], index_col=None)
    df = dfraw[dfraw.estado.notna() &
           dfraw.municipio.isna() &
           dfraw.codmun.isna()][['regiao','estado', 'data', 'casosAcumulado', 'obitosAcumulado']]
    df = df.rename(columns = {'obitosAcumulado':'obitosAcumulados', 'casosAcumulado':'casosAcumulados'})
    df = df.assign(casosNovos = '', obitosNovos = '')[['regiao','estado','data',
                                              'casosNovos', 'casosAcumulados','obitosNovos',
                                             'obitosAcumulados']]
    df[df.data == reportdate[-4:]+'-'+reportdate[2:4]+'-'+reportdate[0:2]].to_csv(f'archive/COVID19_{reportdate}_diario.csv', float_format="%.0f", sep = ';', index = False)
    print(f'SAVED: brasil/archive/COVID19_{reportdate}_diario.csv')
    print('Gootables: ' + str(validate(f'archive/COVID19_{reportdate}_diario.csv')['valid']))
    print(df[df.data == reportdate[-4:]+'-'+reportdate[2:4]+'-'+reportdate[0:2]][['casosAcumulados', 'obitosAcumulados']].aggregate('sum'))

def update():
    list_of_files = glob.glob('archive/*')
    latest_file = max(list_of_files, key=os.path.getctime)
    print(f'\nO arquivo mais atual é: {latest_file}')
    if filecmp.cmp(latest_file, 'latest.csv'):
        print('\nLatest file on covid.saude.gov.br is the same: Equals')
    else:
        print('\nThe files are different. Copying...')
        copyfile(latest_file, 'latest.csv')
        print('\nReading to convert')
        with open('latest.csv', encoding='utf-8') as f:
            data = f.read()
        print('\nConverting to utf-8')
        with open('latest.csv', 'w', encoding='utf-8') as f:
            f.write(data)
        f.close()
        #value_when_true if condition else value_when_false
        print('\nHeader: OK') if checkHeader('latest.csv') else print('\h|!| Header is different!')
        print('Gootables: ' + str(validate('latest.csv')['valid']))
        checkBoletim('latest.csv')
        gate = input('\n| ? | Deseja commitar e enviar para o gitlab? y/n')
        if gate == 'y':
            latest_file_name = latest_file.split('archive\COVID19_')[1].replace('.csv','')
#             print(f'update brasil {latest_file_name}')
            git_push('../.git', f'update brasil {latest_file.split(".")[0]}')
        print('\nThe End!')

def checkBoletim(file):
    import pandas as pd
    df = pd.read_csv(file, sep = ';', parse_dates = ['data'])
    print(f'Últimos dados: {df.data.max()}')
    result = df[df['data'] == df.data.max()].drop(['regiao', 'estado', 'data'], axis = 1).aggregate(['sum']).to_dict()
    casosNovos = result['casosNovos']['sum']
    casosAcumulados = result['casosAcumulados']['sum']
    obitosNovos = result['obitosNovos']['sum']
    obitosAcumulados = result['obitosAcumulados']['sum']
    print(f'\nCasos novos: {casosNovos:,}\tCasos: {casosAcumulados:,}\nÓbitos novos: {obitosNovos:,}\tObitos: {obitosAcumulados:,}')
      

def checkHeader(file):
    with open(file, encoding='utf-8') as f:
        data = f.readline()
        f.close()
        return(data.find('regiao;estado;data;casosNovos;casosAcumulados;obitosNovos;obitosAcumulados') != -1)

def git_push(repository_file, commit_message):
    try:
        repo = Repo(repository_file)
        repo.index.add(['brasil/latest.csv'])
        repo.index.commit(commit_message)
        origin = repo.remote(name='origin')
        origin.push()
    except:
        print('| ! | Some error occured while pushing the code')    

# git_push()

# newdrv = createDriver()
# download(newdrv)
# update()
reportdate = date.today().strftime("%d%m%Y")
extrairDiario(reportdate)