# covid19-sesa-paraná

Publicação da raspagem dos boletins diários da Sesa-PR (informe epidemiológico) e do Ministério da Saúde (dados do painel covid).

## sesa-pr

Dados raspados dos informes publicados em [site sesa](https://www.saude.pr.gov.br/Pagina/Coronavirus-COVID-19).

### base de casos, óbitos, recuperados e em investigação Sesa-PR

[CSV com todos os registros desde o início, por data e município IBGE](base_sesapr.csv)


### base de leitos Sesa-PR

[CSV com todos os registros de ocupação e estoque, desde o ínicio, por tipo de leitos e macroregião da Saúde da Sesa-PR](leitospr.csv)


## Ministério da Saúde

Dados raspados dos boletins diários em [portal covid19](https://covid.saude.gov.br/)

### resultados por UF

[CSV com todos os registros oficias, por UF](brasil/latest.csv)


## Referências

- [Painel Nacional Consolidado das Secretarias](https://labs.wesleycota.com/sarscov2/br/) e [Página no Github](https://github.com/wcota/covid19br)
- [Brasil.io esforço colaborativo](https://brasil.io/dataset/covid19/caso)
- [Conselho Nacional de Secretários de Saúde - Painel Covid](https://www.conass.org.br/painelconasscovid19/)
