## Backlog

Algumas correções necessárias a serem feitas:

- [ ] 08/08, Sesa declara que não irá divulgar dados por incosistências.
- [ ] 05/08, recuperados não raspado, pdf com imagem
- [ ] 30/07, recuperados não raspado, pdf com imagem
- [ ] 23/07, somas incorretas, utilizado csv
- [ ] 21/07, em investigação com soma divergente
- [ ] 14/07 soma de todas a colunas no PDF deram errado. dados extraídos do CSV, investigacao e recuperados zerados
- [ ] 12/07 uma das páginas tinha a coluna de recuperados sem dados, o que prejudica a consistência da série. Foi inteira zerada.
- [ ] 30/06 dados errados de casos e óbitos no pdf, embora a soma final estivesse certa com o csv. outras colunas zeradas para evitar mais erros
- [ ] em_análise 19/06: soma não bate
- [ ] recuperados em 09/06: dado fico como imagem dentro de uma tabela onde os outros dados não eram imagem, não foi possível recuperar
- [ ] divergência da soma de recuperados em 21/05, 23/05, 31/05
- [ ] 27/03, manter em _vazio_ o número de casos descartados e em_investigacao ao invés de _0_, pois não foram divulgados
- [ ] Entre 16/04 e 27/04, os dados de descartados e em_investigacao não foram divulgados, sugestão é manter em _vazio_ ao invés de _0_.
- [ ] Em investigação em 02/05, últimas páginas do PDF
- [ ] Em investigação em 03/05, últimas páginas do PDF
- [ ] Em investigação em 04/05, últimas páginas do PDF
- [ ] Em investigação em 05 e 06/05, últimas páginas do PDF
- [ ] Em 28/04 é retomada a divulgação dos casos em_investigacao desagregados, ainda não foram lidos e computados na base, porém se encontram no INFORME_EPIDEMIOLOGICO_28_04_2020.pdf, em tabela separada nas últimas páginas.
- [ ] Checar eventuais inconsistências triangulando com a base do Brasil.io
